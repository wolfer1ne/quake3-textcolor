# enemy_territory.py
# ────────────────────────────────────────────────────────────
# § Summary
# Enemy territory color codes
# ────────────────────────────────────────────────────────────
# § Informations
# File    : color_codes/enemy_territory.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-14 18:36:27
# Time-stamp: <2018-05-16 16:38:43 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# "https://github.com/etlegacy/etlegacy/blob/master/src/qcommon/q_math.h#L145"
# ────────────────────────────────────────────────────────────


from enum import Enum, unique


_ENEMY_TERRITORY_COLOR_CHARACTERS = ':,=>?ABC'


@unique
class colors (Enum):
    '''OSP color codes. See link in comment'''
    BLACK    = 0
    RED      = 1
    GREEN    = 2
    YELLOW   = 3
    BLUE     = 4
    CYAN     = 5
    MAGENTA  = 6
    WHITE    = 7
    ORANGE   = 8
    MDGREY   = 9
    LTGREY   = ':'
    MDGREEN  = '<'
    MDYELLOW = '='
    MDBLUE   = '>'
    MDRED    = '?'
    LTORANGE = 'A'
    MDCYAN   = 'B'
    MDPURPLE = 'C'


    @classmethod
    def get (this, color):
        '''Return color code'''
        if (isinstance (color, int)):
            return this (color % len (this))
        elif (isinstance (color, str)):
            if (color in _ENEMY_TERRITORY_COLOR_CHARACTERS):
                return colors (color)
            elif (color.isdigit()):
                return this (int (color) % len (this))
            else:
                value = sum (ord (x) for x in color)
                return this (value % len (this))
