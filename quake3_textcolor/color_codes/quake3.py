# quake3.py
# ────────────────────────────────────────────────────────────
# § Summary
# Original Quake III color codes
# ────────────────────────────────────────────────────────────
# § Informations
# File    : color_codes/quake3.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-14 18:17:55
# Time-stamp: <2018-05-16 16:29:41 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# This color codes will be used as default ones
# ────────────────────────────────────────────────────────────


from enum import Enum, unique


@unique
class colors (Enum):
    '''Original Quake III color codes'''
    BLACK   = 0
    RED     = 1
    GREEN   = 2
    YELLOW  = 3
    BLUE    = 4
    CYAN    = 5
    MAGENTA = 6
    WHITE   = 7


    @classmethod
    def get (this, color):
        '''Return color code'''
        if (isinstance (color, int)):
            return this (color % len (this))
        elif (isinstance (color, str) and (color.isdigit())):
            return this (int (color) % len (this))
        else:
            value = sum (ord (x) for x in color)
            return this (value % len (this))
