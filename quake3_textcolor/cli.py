# cli.py
# ────────────────────────────────────────────────────────────
# § Summary
# Command line interface stuff
# ────────────────────────────────────────────────────────────
# § Informations
# File    : cli.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-15 20:45:30
# Time-stamp: <2018-05-16 19:50:02 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# * argument parser is defined here
# ────────────────────────────────────────────────────────────


import argparse


def define_parser (description, version):
    '''Returns default parser used by this script'''
    parser = argparse.ArgumentParser (description = description,
                                      epilog      = "NOTE: When using cmd.exe with not quoted text use \'^^\'")

    parser.add_argument ('text',
                         type  = str,
                         nargs = '+',
                         help  = "Text to be colored")

    parser.add_argument ('--colors',
                         type    = str,
                         choices = ['q3', 'et'],
                         default = 'q3',
                         help    = "Color codes used in text (Default = '%(default)s')")

    parser.add_argument ('--output',
                         type    = str,
                         choices = ['console', 'html', 'latex'],
                         default = 'console',
                         help    = "Specifies output format (Default = '%(default)s')")

    parser.add_argument ('--separator',
                         type    = str,
                         default = ' ',
                         help    = "String used as separator when arguments are given in list - not quoted. (Default = '%(default)s')")

    parser.add_argument ('--version',
                         action  = 'version',
                         version = '%(prog)s ' + version)

    return parser
