# -*- coding: utf-8 -*-
__summary__   = "Quake III-like console text formatter"
__project__   = "quake3-textcolor"
__author__    = "Krystian Jedziniak (wolfer1ne)"
__copyright__ = "Copyright © 2018 by Krystian Jedziniak"
__license__   = "MIT"
__version__   = "0.3.0"
__doc__       = "Script for printing color messages in Quake III-like style"
