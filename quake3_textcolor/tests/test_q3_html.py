# test_q3_html.py
# ────────────────────────────────────────────────────────────
# § Summary
# Test for Quake III HTML output
# ────────────────────────────────────────────────────────────
# § Informations
# File    : tests/test_q3_html.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-17 16:00:31
# Time-stamp: <2018-05-17 20:11:16 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────



import unittest


import q3txtclr
from color_codes.quake3 import colors as q3


class Tester (unittest.TestCase):

    def setUp (this):
        this.flags = '--output=html'

    def test_black_color (this):
        '''Quake III Black color, HTML output'''

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = [this.flags, '^0ZZ']),
                          '<font color=#000000>ZZ</font>')


    def test_two_colors_html (this):
        '''Quake III Green and Black colors, HTML output'''

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = [this.flags, '^2GG ^0BB']),
                          '<font color=#00ff00>GG </font><font color=#000000>BB</font>')


    def test_sign_as_color (this):
        '''Quake III Green color as sign (Z), HTML output'''

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = [this.flags, '^ZAA']),
                          '<font color=#00ff00>AA</font>')


if __name__ == '__main__':
    unittest.main()
