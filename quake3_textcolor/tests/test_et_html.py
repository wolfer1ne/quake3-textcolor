# test_et_html.py
# ────────────────────────────────────────────────────────────
# § Summary
# Test for Enemy territory HTML output
# ────────────────────────────────────────────────────────────
# § Informations
# File    : tests/test_et_html.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-17 19:04:46
# Time-stamp: <2018-05-17 20:13:22 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────



import unittest


import q3txtclr
from color_codes.enemy_territory import colors as et
from palettes.enemy_territory_rgb import colors as pal


class Tester (unittest.TestCase):

    def setUp (this):
        this.flags = ['--colors=et', '--output=html']

    def test_orange_color (this):
        '''ET Orange Ccolor, HTML output'''
        args = this.flags
        args.append('^8ORANGE')
        expected = '<font color=#' + pal[et.ORANGE] + '>ORANGE</font>'

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = args), expected)

    def test_sign_as_color (this):
        '''ET MDRED color as sign (?), HTML output'''
        args = this.flags
        args.append('^?MDRED')
        expected = '<font color=#' + pal[et.MDRED] + '>MDRED</font>'

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = args), expected)
