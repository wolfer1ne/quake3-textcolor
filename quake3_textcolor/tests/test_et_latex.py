# test_et_latex.py
# ────────────────────────────────────────────────────────────
# § Summary
# Test for Enemy territory LaTeX output
# ────────────────────────────────────────────────────────────
# § Informations
# File    : tests/test_et_latex.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-17 19:50:19
# Time-stamp: <2018-05-17 20:06:15 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────



import unittest


import q3txtclr
from color_codes.enemy_territory import colors as et


class Tester (unittest.TestCase):

    def setUp (this):
        this.flags = ['--colors=et', '--output=latex']

    def test_orange_color (this):
        '''ET orange color, Latex output'''
        args = this.flags
        args.append('^8ORANGE')
        expected = r'\textcolor{Orange}{ORANGE}'

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = args), expected)

    def test_sign_as_color (this):
        '''ET MDRED(?) color, Latex output '''
        args = this.flags
        args.append('^?MDRED')
        expected = r'\textcolor{OrangeRed}{MDRED}'

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = args), expected)
