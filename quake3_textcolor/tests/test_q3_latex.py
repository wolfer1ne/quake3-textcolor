# test_q3_latex.py
# ────────────────────────────────────────────────────────────
# § Summary
# Test for Quake III LaTeX output
# ────────────────────────────────────────────────────────────
# § Informations
# File    : tests/test_q3_latex.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-17 19:43:34
# Time-stamp: <2018-05-17 20:09:21 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────



import unittest


import q3txtclr
from color_codes.quake3 import colors as q3


class Tester (unittest.TestCase):

    def setUp (this):
        this.flags = '--output=latex'

    def test_black_color (this):
        '''Quake III black color, Latex output'''

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = [this.flags, '^0ZZ']),
                          r'\textcolor{Black}{ZZ}')


    def test_two_colors_html (this):
        ''' Quake III Green and black colors, Latex output'''

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = [this.flags, '^2GG ^0BB']),
                          r'\textcolor{Green}{GG }\textcolor{Black}{BB}')


    def test_sign_as_color (this):
        '''Quake III Green color as sign (Z), Latex output'''

        print (this.shortDescription())
        this.assertEqual (q3txtclr.main (argv = [this.flags, '^ZAA']),
                          r'\textcolor{Green}{AA}')
