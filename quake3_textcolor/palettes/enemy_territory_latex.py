# enemy_territory_latex.py
# ────────────────────────────────────────────────────────────
# § Summary
# LaTeX color symbol names for Enemy territory color codes
# ────────────────────────────────────────────────────────────
# § Informations
# File    : palettes/enemy_territory_latex.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-16 17:12:16
# Time-stamp: <2018-05-23 19:53:08 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# "https://en.wikibooks.org/wiki/LaTeX/Colors"
# TODO: MD,LTGRAY
# ────────────────────────────────────────────────────────────



from quake3_textcolor.color_codes.enemy_territory import colors as et


colors = {
    (et.BLACK)    : ("Black"),
    (et.RED)      : ("Red"),
    (et.GREEN)    : ("Green"),
    (et.YELLOW)   : ("Yellow"),
    (et.BLUE)     : ("Blue"),
    (et.CYAN)     : ("Cyan"),
    (et.MAGENTA)  : ("Magenta"),
    (et.WHITE)    : ("White"),
    (et.ORANGE)   : ("Orange"),
    (et.MDGREY)   : ("Gray"),
    (et.LTGREY)   : ("Gray"),
    (et.MDGREEN)  : ("LimeGreen"),
    (et.MDYELLOW) : ("Goldenrod"),
    (et.MDBLUE)   : ("NavyBlue"),
    (et.MDRED)    : ("OrangeRed"),
    (et.LTORANGE) : ("YellowOrange"),
    (et.MDCYAN)   : ("SkyBlue"),
    (et.MDPURPLE) : ("Periwinkle")
}
