# enemy_territory_con.py
# ────────────────────────────────────────────────────────────
# § Summary
# Console colors for Enemy territory color codes
# ────────────────────────────────────────────────────────────
# § Informations
# File    : pallettes/enemy_territory_con.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-16 18:34:00
# Time-stamp: <2018-05-23 19:52:31 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────


from colorama import Fore, Style


from quake3_textcolor.color_codes.enemy_territory import colors as et


colors = {
    (et.BLACK)    : (Fore.BLACK),
    (et.RED)      : (Fore.RED),
    (et.GREEN)    : (Fore.GREEN),
    (et.YELLOW)   : (Fore.YELLOW),
    (et.BLUE)     : (Fore.BLUE),
    (et.CYAN)     : (Fore.CYAN),
    (et.MAGENTA)  : (Fore.MAGENTA),
    (et.WHITE)    : (Fore.WHITE  + Style.BRIGHT),
    (et.ORANGE)   : (Fore.YELLOW + Style.BRIGHT),
    (et.MDGREY)   : (Fore.WHITE  + Style.DIM),
    (et.LTGREY)   : (Fore.WHITE),
    (et.MDGREEN)  : (Fore.GREEN  + Style.DIM),
    (et.MDYELLOW) : (Fore.YELLOW + Style.DIM),
    (et.MDBLUE)   : (Fore.BLUE   + Style.DIM),
    (et.MDRED)    : (Fore.RED    + Style.DIM),
    (et.LTORANGE) : (Fore.YELLOW + Style.BRIGHT),
    (et.MDCYAN)   : (Fore.CYAN   + Style.DIM),
    (et.MDPURPLE) : (Fore.CYAN   + Style.DIM)
}
