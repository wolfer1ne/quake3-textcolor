# enemy_territory_rgb.py
# ────────────────────────────────────────────────────────────
# § Summary
# RGB colors for Enemy Territory palette
# ────────────────────────────────────────────────────────────
# § Informations
# File    : palettes/enemy_territory_rgb.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-14 22:17:59
# Time-stamp: <2018-05-23 19:53:01 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# "https://github.com/etlegacy/etlegacy/blob/master/src/qcommon/q_math.h#L121"
# ────────────────────────────────────────────────────────────



from quake3_textcolor.color_codes.enemy_territory import colors as et


colors = {
    (et.BLACK)    : ("000000"),
    (et.RED)      : ("ff0000"),
    (et.GREEN)    : ("00ff00"),
    (et.YELLOW)   : ("ffff00"),
    (et.BLUE)     : ("0000ff"),
    (et.CYAN)     : ("00ffff"),
    (et.MAGENTA)  : ("ff00ff"),
    (et.WHITE)    : ("ffffff"),
    (et.ORANGE)   : ("ffa500"),
    (et.MDGREY)   : ("a9a9a9"),
    (et.LTGREY)   : ("d3d3d3"),
    (et.MDGREEN)  : ("11cd7a"),
    (et.MDYELLOW) : ("f1de11"),
    (et.MDBLUE)   : ("0377c8"),
    (et.MDRED)    : ("f3464a"),
    (et.LTORANGE) : ("ef7633"),
    (et.MDCYAN)   : ("48d1cc"),
    (et.MDPURPLE) : ("3911a0")
}
