# quake3_con.py
# ────────────────────────────────────────────────────────────
# § Summary
# Console colors for Quake III color codes
# ────────────────────────────────────────────────────────────
# § Informations
# File    : palettes/quake3_con.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-16 18:30:42
# Time-stamp: <2018-05-23 19:53:30 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────


from colorama import Fore, Style


from quake3_textcolor.color_codes.quake3 import colors as q3


colors = {
    (q3.BLACK)   : (Fore.BLACK),
    (q3.RED)     : (Fore.RED),
    (q3.GREEN)   : (Fore.GREEN),
    (q3.YELLOW)  : (Fore.YELLOW),
    (q3.BLUE)    : (Fore.BLUE),
    (q3.CYAN)    : (Fore.CYAN),
    (q3.MAGENTA) : (Fore.MAGENTA),
    (q3.WHITE)   : (Fore.WHITE + Style.BRIGHT)
}
