# quake3_latex.py
# ────────────────────────────────────────────────────────────
# § Summary
# LaTeX color symbol names for Quake III color codes
# ────────────────────────────────────────────────────────────
# § Informations
# File    : palettes/quake3_latex.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-16 17:09:12
# Time-stamp: <2018-05-23 19:53:41 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# "https://en.wikibooks.org/wiki/LaTeX/Colors"
# ────────────────────────────────────────────────────────────



from quake3_textcolor.color_codes.quake3 import colors as q3


colors = {
    (q3.BLACK)   : ("Black"),
    (q3.RED)     : ("Red"),
    (q3.GREEN)   : ("Green"),
    (q3.YELLOW)  : ("Yellow"),
    (q3.BLUE)    : ("Blue"),
    (q3.CYAN)    : ("Cyan"),
    (q3.MAGENTA) : ("Magenta"),
    (q3.WHITE)   : ("White")
}
