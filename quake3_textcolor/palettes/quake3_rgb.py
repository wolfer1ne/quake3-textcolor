# quake3_rgb.py
# ────────────────────────────────────────────────────────────
# § Summary
# RGB colors for Quake III palette
# ────────────────────────────────────────────────────────────
# § Informations
# File    : palettes/quake3_rgb.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-14 21:56:44
# Time-stamp: <2018-05-23 19:53:50 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────



from quake3_textcolor.color_codes.quake3 import colors as q3


colors = {
    (q3.BLACK)   : ("000000"),
    (q3.RED)     : ("ff0000"),
    (q3.GREEN)   : ("00ff00"),
    (q3.YELLOW)  : ("ffff00"),
    (q3.BLUE)    : ("0000ff"),
    (q3.CYAN)    : ("00ffff"),
    (q3.MAGENTA) : ("ff00ff"),
    (q3.WHITE)   : ("ffffff")
}
