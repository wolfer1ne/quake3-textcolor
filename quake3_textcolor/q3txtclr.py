#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from quake3_textcolor import metadata
# q3txtclr.py
# ────────────────────────────────────────────────────────────
# § Summary
__summary__   = metadata.__summary__
# ────────────────────────────────────────────────────────────
# § Informations
__file__      = "main.py"
__project__   = metadata.__project__
__author__    = metadata.__author__
__copyright__ = metadata.__copyright__
__license__   = metadata.__license__
# ────────────────────────────────────────────────────────────
__created__   = "2018-05-10 15:06:38"
__last_edit__ = "Time-stamp: <2018-05-23 19:59:44 (wolfer1ne)>"
__version__   = metadata.__version__
# ────────────────────────────────────────────────────────────
# § Commentary
__doc__       = metadata.__doc__
# ────────────────────────────────────────────────────────────



import sys
import re


from quake3_textcolor import cli
from quake3_textcolor.color_codes import quake3, enemy_territory
from quake3_textcolor.palettes    import quake3_con, quake3_rgb, quake3_latex, enemy_territory_con, enemy_territory_rgb, enemy_territory_latex
from quake3_textcolor.generators  import html5, latex, console


# How color sequence starts
# '^^' inserts '^'
ESCAPE_COLOR_SIGN = '^'

# '^x' where x is single character
# also when splitting keep that sequence
REGEX_PATTERN     = '(\^.)'

# Similar to ‘REGEX_PATTERN’ but do not save sequence
COLOR_REGEX_PATTERN = '\^.'


def convert_color_sequences (tokens, get_color):
    '''Convert strings like '^x' to colors from palette.
`tokens' is a splitted list
`get_color' is a function that is used to replace color sequence with object.
Argument is a single character
'''

    colors_regex = re.compile (COLOR_REGEX_PATTERN)

    for i, clr in enumerate (tokens):
        if (re.match (colors_regex, clr)):
            tokens[i] = get_color (clr.strip (ESCAPE_COLOR_SIGN))


def get_color_code (code):
    '''Return color code objects'''
    if code == 'q3':
        return quake3.colors
    elif code == 'et':
        return enemy_territory.colors
    else:
        raise ValueError('Only `et\' and `et\' color codes are supported!')


def _palette_for_q3 (output_style):
    _palettes = {
        'html'    : quake3_rgb.colors,
        'latex'   : quake3_latex.colors,
        'console' : quake3_con.colors
    }

    return _palettes[output_style]


def _palette_for_et (output_style):
    _palettes = {
        'html'    : enemy_territory_rgb.colors,
        'latex'   : enemy_territory_latex.colors,
        'console' : enemy_territory_con.colors
    }

    return _palettes[output_style]

def get_palette_for_color_code (code, output_style):
    if output_style not in ['html', 'latex', 'console']:
        raise ValueError('Only `html\', `latex\' and `console\' output styles are supported!')

    if code == 'q3':
        return _palette_for_q3 (output_style)
    elif code == 'et':
        return _palette_for_et (output_style)
    else:
        raise ValueError('Only `q3\', and `et\' options are supported!')


def get_generator (style):
    if style not in ['html', 'latex', 'console']:
        raise ValueError('Only `html\', `latex\' and `console\' options are supported!')

    _generators = {
        'html'    : html5.generate,
        'latex'   : latex.generate,
        'console' : console.generate
    }

    return _generators[style]


def main (argv = None):
    parser = cli.define_parser (metadata.__summary__,
                                metadata.__version__)

    if argv is None:
        # skip script path
        argv = sys.argv[1:]

    if (len (argv) == 0):
        parser.print_help(sys.stderr)
        sys.exit(1)

    # parse given arguments
    args = parser.parse_args(argv)

    # convert input from command line into one string
    arguments = args.separator.join (args.text)

    # 'tokenize'
    arguments = re.split (REGEX_PATTERN, arguments)

    # replace '^^' with '^'
    arguments = [ESCAPE_COLOR_SIGN if x == 2*ESCAPE_COLOR_SIGN else x for x in arguments]

    # if not specified, defaults are used
    color_code = args.colors

    # use specified color codes with appropiate palette
    color_code = get_color_code (args.colors)
    palette    = get_palette_for_color_code (args.colors, args.output)

    convert_color_sequences (arguments, color_code.get)

    generator = get_generator (args.output)

    ret = generator (arguments,
                     color_code,
                     color_code.get,
                     palette)

    # Fixes printing None
    if args.output != 'console':
        print (ret)
    return ret


if __name__ == "__main__":
    sys.exit (main())
