# console.py
# ────────────────────────────────────────────────────────────
# § Summary
# Colored output on console
# ────────────────────────────────────────────────────────────
# § Informations
# File    : generators/console.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# § Additional informations
# * Colorama
# Copyright (c) 2010 Jonathan Hartley (See README.rst)
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-16 17:53:34
# Time-stamp: <2018-05-17 21:27:03 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
# All is handled by ‘Colorama’
# ────────────────────────────────────────────────────────────


from colorama import init, Fore, Back, Style


def generate (tokens, color_codes, get_color, palette):
    '''Print colored sequence to console output'''
    # from colorama
    init()

    for i in tokens:
        if not isinstance (i, color_codes):
            print (i, end = '')
        else:
            print (palette[i], end = '')
    else:
        print (Style.RESET_ALL + '', flush = True)
