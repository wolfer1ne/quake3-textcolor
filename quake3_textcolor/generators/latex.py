# latex.py
# ────────────────────────────────────────────────────────────
# § Summary
# LaTeX output generator
# ────────────────────────────────────────────────────────────
# § Informations
# File    : generators/latex.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-16 17:24:28
# Time-stamp: <2018-05-16 17:30:31 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────




# LaTeX tags for colors
# \textcolor{CLR}{TEXT}
# \\, ‘{’ with ‘}’ have to be escaped
LATEX_OPEN_TAG = '\\textcolor{{{}}}{{'
LATEX_CLOSE_TAG = '}'


def generate (tokens, color_codes, get_color, palette):
    '''Generate LaTeX output from `tokens'
`get_color' is a function that takes one argument
and returns single color
'''
    retval = ""

    current_color = None
    last_color    = None

    for i in tokens:
        if not isinstance (i, color_codes):
            retval += i
        else:
            if (current_color == None):
                current_color = i
            else:
                retval += LATEX_CLOSE_TAG
                last_color = current_color
                current_color = i

            retval += LATEX_OPEN_TAG.format (palette[i])
    else:
        if (current_color != None):
            retval += LATEX_CLOSE_TAG

    return retval
