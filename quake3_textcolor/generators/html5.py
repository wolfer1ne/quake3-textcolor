# html5.py
# ────────────────────────────────────────────────────────────
# § Summary
# HTML output generator
# ────────────────────────────────────────────────────────────
# § Informations
# File    : generators/html5.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-15 14:50:04
# Time-stamp: <2018-05-16 16:29:11 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────




# HTML tags for colors
HTML_OPEN_TAG  = '<font color=#{}>'
HTML_CLOSE_TAG = '</font>'


def generate (tokens, color_codes, get_color, palette):
    '''Generate HTML output from `tokens'
`get_color' is a function that takes one argument
and returns single color
'''
    retval = ""

    current_color = None
    last_color    = None

    for i in tokens:
        if not isinstance (i, color_codes):
            retval += i
        else:
            if (current_color == None):
                current_color = i
            else:
                retval += HTML_CLOSE_TAG
                last_color = current_color
                current_color = i

            retval += HTML_OPEN_TAG.format (palette[i])
    else:
        if (current_color != None):
            retval += HTML_CLOSE_TAG

    return retval
