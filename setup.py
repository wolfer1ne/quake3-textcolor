# setup.py
# ────────────────────────────────────────────────────────────
# § Summary
# Installation file
# ────────────────────────────────────────────────────────────
# § Informations
# File    : setup.py
# Project : quake3-textcolor
# Author  : Krystian Jedziniak (wolfer1ne)
# Copyright © 2018 by Krystian Jedziniak
# License : MIT
# ────────────────────────────────────────────────────────────
# Created   :  2018-05-17 20:42:04
# Time-stamp: <2018-05-23 20:15:50 (wolfer1ne)>
# ────────────────────────────────────────────────────────────
# § Commentary
#
# ────────────────────────────────────────────────────────────



from setuptools import setup, find_packages


metadata = {}
with open ("./quake3_textcolor/metadata.py") as fp:
    exec (fp.read(), metadata)


with open ("README.rst", 'r') as f:
    long_description = f.read ()


setup (
    name             = 'quake3_textcolor',
    version          = metadata['__version__'],
    description      = long_description,
    author           = metadata['__author__'],
    author_email     = '',
    url              = 'https://gitlab.com/wolfer1ne/quake3-textcolor',
    packages         = find_packages (),
    install_requires = ['colorama'],
    entry_points     = {
        'console_scripts': ['q3txtclr = quake3_textcolor.quake3_textcolor:main']
    }
)
