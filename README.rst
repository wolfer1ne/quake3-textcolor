==================
 Quake3-textcolor
==================

Script for printing color messages in Quake III-like style

* Installation

  This project is currently not available by `PyPI <https://pypi.org/>`_, so you have to download it with by using longer commands :(

  Assuming you have already installed `Python <http://www.python.org/>`_ with correctly set ``PATH`` variable, commands below *should* work (`Git is required <https://git-scm.com/>`_):

  - Windows

    .. code-block:: text

                    pip install git+https://gitlab.com/wolfer1ne/quake3-textcolor.git

  - Linux based systems

    .. code-block:: text

                    pip install git+https://gitlab.com/wolfer1ne/quake3-textcolor.git --user

    ``--user`` option installs package locally (sudo is not required)

* Usage

  For Windows and Linux-based systems:

  .. code-block:: text

                  python -m quake3_textcolor [--colors] [--output] TEXT

* Supported options

  - Color codes

    .. code-block:: text

                --colors=q3 for Quake III color codes or
                --colors=et for Enemy Territory color codes

  - Output formats

    Currently three formats are supported:

    - console (Default)
    - html
    - latex

    To specify for instance html output use:

    .. code-block:: text

       --output=html

  - Separator

    If you will enter color text not inside quotes, for instance ``"^1 TEST"`` but like this ``^1 TEST (no quotes around)``, all arguments will be separated by default by space.

    If different separator is needed use example below:

    .. code-block:: text

       --separator=__ TEST TEST -> TEST__TEST
       --separator= TEST TEST   -> TESTTEST (space after `=')

=========
 LICENSE
=========

See LICENSE file for the MIT License

-----------------------------
colorama (External depedency)
-----------------------------

Copyright (c) 2010 Jonathan Hartley
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holders, nor those of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

